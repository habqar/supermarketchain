# Fruit Supermarket Chain Test

A national supermarket chain is interested in starting special offers in their stores. They made a special offers.

- Buy One get One free of apple.
- Three for the price of two of Watermelon.

We want to calculate the total price discouted of the basket.
We took as an example : 4 apples - 3 Oranges - 5 Watermelon


# Technical Description 

## Initals

This project is made by spring boot to reduce time and increase productivity and to get a prototype easier to maintain. (As a first sprint)
- I used Java 8 streaming to in-memory calculation.
- I used Swagger for the documentation the endpoint.
- I used lombok to spicy up and reduce code (no need to write getter and setter ..)
- I used SLf4j logs to log the steps of the executions.


For this example, I used a conception that can be improved easily in another sprint for these cases :

- Having a lot of Fruit Item (example 50 fruits) : Using external file base for all items with the prices instead of using Enumerator.  I used enums for item because can only take one out of a small set of possible values.
- Having more discounting offers.

## Dependencies

- Maven : Software project management and comprehension too
- Lombok : Spicing up the code.
- Spring-boot-starter : Starter for building web, including RESTful, applications using _Spring_ mvc.
- Swagger : Rendering documentation for an API defined with the OpenAPI (Swagger) specification.
- Junit : Creating an up-to-date foundation for developer-side testing on the JVM.

## Execution 

### Using browser IHM

- Clone the project in your local machine and execute this command :
```
mvn spring-boot:run
```
- Brows to *http://localhost:8080/swagger-ui.html*  

- Provide the quantity wanted of each fruit item 

![swagger interface](files/swagger-ui_1.PNG)

The result is :

![swagger result interface](files/swagger-ui_2.PNG)

### Using end-to-end Test

Run the end-to-end test using the command :
```
mvn test
```

- This is a end-to-end test made for the example shown in the document.
Go to the *DiscountingCoreTest* test class and change the example :

![Testing code](files/test-code.PNG)

- The log of execution

![Testing log](files/end-to-end-log.PNG)

## Developer

Hamza Abqar : abqar.hamza@gmail.com