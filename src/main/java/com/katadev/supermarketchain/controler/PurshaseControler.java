package com.katadev.supermarketchain.controler;

import com.katadev.supermarketchain.core.DiscountingCore;
import com.katadev.supermarketchain.model.Basket;
import com.katadev.supermarketchain.model.Item;
import com.katadev.supermarketchain.model.ItemPack;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;

@RestController
public class PurshaseControler {

    private final DiscountingCore discountingCore;

    @Autowired
    public PurshaseControler(DiscountingCore discountingCore) {
        this.discountingCore = discountingCore;
    }

    @GetMapping("/purshase")
    public Basket purshaseFruit(
            @RequestParam int appleQuantity,
            @RequestParam int orangeQuantity,
            @RequestParam int waterMelonQuantity){
        List<ItemPack> itemPacks = Arrays.asList(
                new ItemPack(appleQuantity, Item.APPLE),
                new ItemPack(waterMelonQuantity,Item.WATERMELON),
                new ItemPack(orangeQuantity,Item.ORANGE));

        return discountingCore.calculatingTotalPrice(itemPacks);
    }

}
