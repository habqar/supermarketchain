package com.katadev.supermarketchain.core;


import com.katadev.supermarketchain.model.Basket;
import com.katadev.supermarketchain.model.ItemPack;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public class DiscountingCore {

    /**
     * The discounted total price to pay is the sum of discountedprice of each item
     *
     * @param itemPacks : Listof Packs of each item
     * @return Basket with the price
     */
    public Basket calculatingTotalPrice(List<ItemPack> itemPacks){
        log.info("Start Calculating Total Price to pay of the Basket ..");
        double totalPrice = itemPacks.parallelStream().peek(this::calculateDiscountingPrice).mapToDouble(ItemPack::getDiscountedPrice).sum();
        log.info("** You have to pay : "+totalPrice + " £ **");
        return Basket.builder().itemPackList(itemPacks).price(totalPrice).build();
    }

    /**
     * The discounted price of the each itemPack is the discountedQuantity * Price
     *
     * @param itemPack One itemPack contain one fruit item
     * @return discountedPrice
     */
    double calculateDiscountingPrice(ItemPack itemPack) {
        log.debug("Calculating Discounted Price of "+ itemPack.getQuantity()+" of ["+ itemPack.getItem().name()+"] ...");
        double price = itemPack.getItem().getPrice() * calculateDiscountedQuantity(itemPack);
        itemPack.setDiscountedPrice(price);
        log.info("Discounted Price of "+ itemPack.getQuantity()+" of ["+ itemPack.getItem().name()+"] is : "+price+ " £");
        return price;
    }

    /**
     * We calculat" the discouted quntity of each fruit.
     * Example for 4 apples : the discounted is quantity is 2
     * Example for 7 watermelon : the discounted quantity is 6*2/3 + 1 = 5
     * @param itemPack itemPack One itemPack contain one fruit item
     * @return discounted quanity of each item
     */
    double calculateDiscountedQuantity(ItemPack itemPack) {
        log.debug("Calculating Discounted Quantity of "+ itemPack.getQuantity()+" of ["+ itemPack.getItem().name()+"] ...");
        double discountedQuantity = itemPack.getQuantity();
        double rest = 0;
        switch (itemPack.getItem()) {
            case APPLE:
                if (itemPack.getQuantity() >= 2)
                    rest = itemPack.getQuantity() % 2; // the rest here can be 0 or 1
                discountedQuantity = (itemPack.getQuantity() - rest )/ 2 + rest;
                break;
            case WATERMELON:
                if (itemPack.getQuantity() >= 3) {
                    rest = itemPack.getQuantity() % 3;
                    discountedQuantity = (itemPack.getQuantity() - rest) * 2 / 3 + rest;
                    break;
                }
        }
        log.debug("Discounted Quantity of "+ itemPack.getQuantity()+" of ["+ itemPack.getItem().name()+"] is "+discountedQuantity);
        return discountedQuantity;
    }
}

