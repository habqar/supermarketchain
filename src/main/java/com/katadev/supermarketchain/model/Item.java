package com.katadev.supermarketchain.model;

import lombok.*;

@AllArgsConstructor
@Getter
public enum Item {
    APPLE(0.2),
    ORANGE(0.5),
    WATERMELON(0.8);

    private double price;
}
