package com.katadev.supermarketchain.model;

import lombok.Data;


@Data
public class ItemPack {
    private int quantity;
    private Item item;
    private double discountedPrice;

    public ItemPack(int quantity, Item item) {
        this.quantity = quantity;
        this.item = item;
    }
}
