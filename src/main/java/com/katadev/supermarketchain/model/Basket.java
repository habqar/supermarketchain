package com.katadev.supermarketchain.model;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class Basket {
    private List<ItemPack> itemPackList;
    private double price;
}
