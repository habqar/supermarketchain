package com.katadev.supermarketchain.core;

import com.katadev.supermarketchain.model.Item;
import com.katadev.supermarketchain.model.ItemPack;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.List;

@SpringBootTest
@RunWith(SpringRunner.class)
public class DiscountingCoreTest {
    private static final double DELTA = 1e-15;
    private List<ItemPack> itemPacks;
    private ItemPack apples, oranges, watermelons;

    @Autowired
    private DiscountingCore discountingCore;

    @Before
    public void init(){
        apples = new ItemPack(4, Item.APPLE);
        oranges = new ItemPack(3, Item.ORANGE);
        watermelons = new ItemPack(5, Item.WATERMELON);
        itemPacks = Arrays.asList(apples, oranges, watermelons);
    }

    @Test
    public void calculatingTotalPrice() {
        Assert.assertEquals(discountingCore.calculatingTotalPrice(itemPacks).getPrice(), 5.1, DELTA);
    }

    @Test
    public void calculateDiscountingPrice() {
        Assert.assertEquals(discountingCore.calculateDiscountingPrice(apples), 0.4, DELTA);
    }

    @Test
    public void calculateDiscountedQuantity() {
        Assert.assertEquals(discountingCore.calculateDiscountedQuantity(watermelons), 4, DELTA);
    }
}
